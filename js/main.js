$(document).ready(function () {


    let $btns = $('.button-group button');


    $btns.click(function (e) {

        $('.project-area .button-group button').removeClass('active');
        e.target.classList.add('active');

        let selector = $(e.target).attr('data-filter');
        $('.project-area .grid').isotope({
            filter: selector
        });

        return false;
    })

    $('.button-group #btn1').trigger('click');

    $('.grid .test-popup-link').magnificPopup({
        type: 'image',
        gallery: { enabled: true }
    });
 

    // Owl-carousel

    $(".owl-carousel").owlCarousel({
        loop: true,
        autoplay: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            560: {
                items: 2
            }
        }
    })

    // sticky navigation menu

    let nav_offset_top = $('.navbar').height() + 50;

    function navbarFixed() {
        if ($('.navbar').length) {
            $(window).scroll(function () {
                let scroll = $(window).scrollTop();
                if (scroll >= nav_offset_top) {
                    $('.main-menu').addClass('navbar_fixed');
                } else {
                    $('.main-menu').removeClass('navbar_fixed');
                }
            })
        }
    }

    navbarFixed();

});